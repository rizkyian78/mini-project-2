const app = require('../server');
const request = require('supertest');
const db = require('../models');

describe('User API Collection ', () => {
    beforeAll(() => {
        db.sequelize.query('TRUNCATE "users", "profiles" RESTART IDENTITY')
    })
    afterAll(() => {
        db.sequelize.query('TRUNCATE "users", "profiles" RESTART IDENTITY')
    })
    let userErrorToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJtYnV0QG1idXQuY29tIiwiaWF0IjoxNTkwOTE2NjY5fQ.fVQL5Qsnk-ROdl1A72b--nWb4iZGXK-8JRBOeqg8-Bk';
    let token;
    let user_id;

    describe('POST user/register', () => {
        // Success Test
        test('should successfully create a new user', done => {
             request(app).post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: "mbut",
                    email: "mbut@mbut.com",
                    password: "123345"
                }).then(res => {
                    const {body, status} = res;
                    expect(status).toEqual(201);
                    expect(body.status).toEqual('Success')
                    done()
                })
        })
        // Error test
        test('should Sucessfully not create User', done => {
            request(app).post('/api/v1/user/register')
            .set('Content-Type', 'application/json')
            .send({
                name: "mbut",
                email: "mbut@mbut.com",
                password: ""
            }).then(res => {
                const {body, status} = res;
                expect(status).toEqual(400);
                expect(body.status).toEqual('Fail');
                done();
            })
        })
    })
    describe('POST user/login', () => {
        // Success Login
        test('should Succesfully login', done => {
            request(app).post('/api/v1/user/login')
            .set('Content-Type', 'application/json')
            .send({
                email: "mbut@mbut.com",
                password: "123345"
            }).then(res => {
                console.log(res)
                const {body, status} = res;
                expect(status).toEqual(200);
                expect(body.status).toEqual('Success');
                token = body.data.token
                done();
            })
        })
        // Error Login
        test('Should not Succesfully Login', done => {
            request(app).post('/api/v1/user/login')
            .set('Content-Type', 'application/json')
            .send({
                email: "mbut13@mbut.com",
                password: "1234"
            }).then(res => {
                const {body, status} = res;
                expect(status).toEqual(403);
                expect(body.status).toEqual('Fail');
                done();
            })
        })
    })
    describe('GET user/profile', () => {
        test('Should Succesfully get user profile', done => {
            request(app).get('/api/v1/user/profile')
            .set('Authorization', token)
            .set('Content-Type', 'application/json')
            .send({
                email: "mbut12@mbut.com",
                password: "123"
            })
            .then(res => {
                const {body, status} = res;
                user_id = body.id
                expect(status).toEqual(200);
                expect(body.status).toEqual('Success');
                done()
            })
        })
    })
    describe('PUT user/update', () => {
        test('Should Sucessfully update user profile', done => {
            request(app).put(`/api/v1/user/update/1`)
            .set('Authorization', token)
            .attach('image', 'uploads/kepala_sekolah.png')
            .then(res => {
                const {body, status} = res;
                expect(status).toEqual(200);
                expect(body.status).toEqual('Success');
                done()
            })
        })
        test('Should Not Succesfully update user profile', done => {
            request(app).put('/api/v1/user/update/1')
            .set('Authorization', token)
            .set('Content-Type', 'mulitpart/form-data')
            .field({name: 'mbutmbutmbut'})
            .attach('image', '')
                .then(res => {
                const {body, status} = res;
                expect(status).toEqual(422);
                expect(body.status).toEqual('Fail');
                done()
            })
        })
    })
})