'use strict';
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    email: {
      type: DataTypes.STRING,
      unique: {
        msg: "Please Use Another Email"
      },
      validate: {
        notEmpty: {
          msg: 'Email Field Still Empty'
        },
        isEmail: {
          msg: "Please Input Email Format"
        }
      },
      },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty:{
          msg: 'Please Input Password'
        }
      }
    },
    role: {
     type: DataTypes.ENUM('Member', 'Admin'),
     defaultValue: 'Member'
    }
    },  {
    hooks: {
      beforeValidate: (instance, option) => {
        instance.email = instance.email.toLowerCase() 
      },
      beforeCreate: (instance, option) => {
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(instance.password, salt);
        instance.password = hashedPassword;
      }
    }
  });
  users.associate = function(models) {
    users.hasOne(models.profile, {
      foreignKey: 'profile_id',
      as: 'owner'
    })
  };
  return users;
};