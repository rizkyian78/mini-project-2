'use strict';
module.exports = (sequelize, DataTypes) => {
  const profile = sequelize.define('profile', {
    name: {
      type: DataTypes.STRING,
    },
    image: {
      type: DataTypes.TEXT,
      defaultValue: 'https://ik.imagekit.io/xn7pla5njr/gerandong_2_KPvYdIs29.png',
    },
    profile_id: DataTypes.INTEGER
  }, {});
  profile.associate = function(models) {
    profile.belongsTo(models.users, {
      foreignKey: 'profile_id',
      as: 'owner'
    })
  };
  return profile;
};