require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

const router = require('./router');
const exceptionHandler = require('./Middleware/exceptionHandler')

app.use(morgan('tiny'))
app.use(cors());
app.use(express.json())

app.use('/api/v1', router)
exceptionHandler.forEach(handler => app.use(handler))

module.exports = app;