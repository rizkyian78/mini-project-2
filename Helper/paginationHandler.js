module.exports = (req, task) => {
    //  Get the page and limit query
    const page = req.query.page;
    const limit = req.query.limit;
    // Get the start of the index
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    // Cut between startIndex and endInde
    const result = task.slice(startIndex, endIndex);
    // Success Handler
    return result;
}