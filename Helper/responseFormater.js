exports.successResponse = function(res, code, data) {
    res.status(code).json({
                 status: "Success",
                 data
                })
};

exports.errorResponse = (res, code, message) => {
    res.status(code).json({
        status: "Fail",
        Message: message
    })
}