const {users, profile} = require('../models');
const bcrypt = require('bcrypt');
const sendgrid = require('@sendgrid/mail');

const {successResponse, errorResponse} = require('../Helper/responseFormater')
const token = require('../Helper/token');
const imagekit = require('../lib/imagekit');

exports.register = async function register(req, res){
    try {
            const user = await users.create(req.body); //email dan pasword
            const profiles = await profile.create({
                name: req.body.name,
                profile_id: user.id
            })
            successResponse(res, 201, {token: token(user)});
        } catch (err) {
            errorResponse(res, 400, err.message);
        };
    };

exports.login = async function login(req, res){
        try {
            const user = await users.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            });
            const isPasswordTrue = await bcrypt.compare(req.body.password, user.password);
            if(!user) errorResponse(res, 403, `${req.body.email} doesn't exist`);
            if(!isPasswordTrue) return errorResponse(res, 403, "Password doesn't Match");
            successResponse(res, 200, {token: token(user)});
        } catch (err) {
            errorResponse(res, 403, [err.message]);
        };
    };
    
exports.profile = async function profile(req, res) {
            const data = await users.findByPk(req.user.id, {
                include: 'owner'
            });
            successResponse(res, 200, data)
    }
exports.update = async function update(req, res) {
        try {
            const image = await imagekit.upload({
                file: req.file.buffer,
                fileName: req.file.originalname
            })
            await profile.update({
                name: req.body.name,
                image: image.url
            },{
                where: {id: req.params.id}
            },{
                include: 'owner'    
            })
            successResponse(res, 200, `id: ${req.params.id} has been updated`);
        } catch (err) {
            errorResponse(res, 422, 'Please Insert Image File');
        };
    };