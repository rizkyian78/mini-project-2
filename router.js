const router = require('express').Router();
const swaggerUi = require('swagger-ui-express');

const uploader = require('./Middleware/uploader');
const authenticate = require('./Middleware/authentication');
const userController = require('./Controller/userController');
const swagger = require('./swagger.json');

router.post('/user/register', userController.register);
router.post('/user/login', userController.login);
router.get('/user/profile',authenticate, userController.profile);
router.put('/user/update/:id',authenticate, uploader.single('image'), userController.update)

// Documentation API endpoint
router.use('/docs',swaggerUi.serve);
router.get('/docs', swaggerUi.setup(swagger));

module.exports = router; 